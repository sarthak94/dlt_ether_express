function setError(where, what) {
	var parent = $(where).parents('.form-group:eq(0)');
	parent.addClass('has-error').append('<span class="help-block">' + what + '</span>');
	toastr.error(what);
}

function unsetError(where) {
	var parent = $(where).parents('.form-group:eq(0)');
	parent.removeClass('has-error').find('.help-block').remove();
	toastr.clear();
}