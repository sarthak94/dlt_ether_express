const mongoose = require('mongoose');
const User = require('../../models/user');
const crypto = require('crypto');
const cryptojs = require("crypto-js");
const moment = require('moment');
const rand = require("random-seed").create()
const ethutil = require("ethereumjs-util")
const path = require('path');
var Web3 = require('web3');
var web3 =new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
// const mailer = require('../../../mailer');


module.exports = {

	//Registration of user along with validations, sending registration mail to user
	create: function(req, res) {
		var data = req.body;

		User.findOne({email : data.email},function(err, users) {
			if (users){
				var response = {
					status:"unsuccess",
					message:"OOps! email already exist, Please try again with another email",
				}
			} else {

				var current_date = (new Date()).valueOf().toString();
				var random = Math.random().toString();
				var unique_code = crypto.createHash('sha1').update(current_date + random).digest('hex');

				var user = new User({
					user_name	:	data.user_name,
					email			:	data.email,
					password	:	data.password,
					profile		: 'avatar1.jpg',
					verified	:	0,
					token			:	unique_code,
				});

				user.save(function(err) {
					if (err) throw err;
				});

				// setup email data with unicode symbols
				let mailOptions = {
					from			:	'"DLT Labs" <hello@dltlabs.io>',
					to				:	 user.email,
					subject		:	'Welcome to DLT ICO | Activate your account.',
					html			:	'Dear '+user.user_name+', <br/><br/>'+
											'Welcome aboard! Confirm your email id to activate your account.<br/><br/>'+
											'Verification Link: <a href="http://ico.dltlabs.io/email-verification?token='+user.token+'">http://ico.dltlabs.io/email-verification?token='+user.token+'</a><br/><br/>'+
											'If you didn\'t create a DLT ICO account, just delete this email and everything will go back to the way it was.<br/><br/>'+
											'Regards <br/>Your Friends <br/>DLT Labs <br/><br/>'+
											'------------------------------------------------------------------------------'
				};

				// // send mail with defined transport object
				// mailer.transporter.sendMail(mailOptions, (error, info) => {
				// 	if (error) {
				// 		return console.log(error);
				// 	}
				// 	console.log('Message %s sent: %s', info.messageId, info.response);
				// });

				response = {
					status:"success",
					message:"You have successfully registered, Check You email.",
				}
			}
			return res.json(response);
		});
	},

	//Authentication of user when the user logs in
	auth: function(req, res) {
		var data = req.body;
		User.findOne({email : data.email, password : data.password},function(err, user) {
			var response = {
				status:"error",
				message:"Credentials not correct.",
			}
			if (user) {
				if (user.verified == 1) {
						req.session.user = user;

						response = {
							status:"success",
							message:"Logged In successfully.",
							data: user.truffle_addr,
						}

					} else {
						response = {
							status:"unsuccess",
							message: "user not verified, Please try again."
						}
					}
			}
			return res.json(response);
		});
	},

	//Updating profile details of the user
	profile: function(req, res) {
		var data = req.body;
		// console.log(data);
		if(data.pre_pass.length) {
			User.findOne({token : data.token, password : data.pre_pass}, function(err, user) {
				var response = {
					status : "error",
					message: "Not a valid user!!",
				}
				if(user) {
					user.user_name = data.user_name;
					if (data.new_pass && data.new_pass.length) {
						user.password = data.new_pass;
					}
					user.save();
					req.session.user = user

					response = {
						status : "success",
						message: "Successfully updated your Changes."
					}
				}
				return res.json(response);
			});
		} else {
			var response = {
				status : "error",
				message: "Password is required."
			}
			return res.json(response);
		}
	},

	//Updating profile image of the user
	ProfileImage: function(req, res) {
		var data = req.body;
		console.log(req.files);

		User.findOne({token : data.token }, function(err, user) {
			var response = {
				status : "error",
				message: "Not a valid user",
			}

			if(user) {
    			var file_ext = path.extname(req.files.profilePic.name);
				// var file_base_name = path.basename(req.files.profilePic.name,file_ext);
					req.files.profilePic.mv('public/img/authors/'+user.sno+file_ext, function(err) {
							if (err) throw err;
						});
				user.profile = user.sno+file_ext;
    			user.save();

    			req.session.user = user

				response = {
					status : "success",
					message: "Successfully uploaded profile image",
				}
			}
			return res.json(response);
		});
	},

	//Verification of email of the user on the basis of token sent via email
	verifyEmail: function(req, res) {
		var token = req.param('token');
		User.findOne({token : token},function(err, user) {
			var response = {
				status:"404",
				message:"token not found.",
			}
			if (user) {
				var current_date = (new Date()).valueOf().toString();
				var random = Math.random().toString();
				var unique_code = crypto.createHash('sha1').update(current_date + random).digest('hex');
				user.token = unique_code;
				user.verified = 1;
				user.save();
				
				response = {
					status: "Success",
					message: "Login to continue"
				}
			}
			return res.status(404).json(response);
		});
	},

	//Creation of truffle account of the user,sending encrypted authorization key via email
	truffleAcc: function(req, res) {
		// req.session.user
		var data = req.body;
		rand.addEntropy(data.sentence);
		var rBytes = new Buffer(32)
		for(var i = 0; i < 32; i++){
			rBytes[i] = rand(256)
		}

		addr = "0x" + ethutil.privateToAddress(rBytes).toString('hex')
		// addr="0xb0ead2c6924eb7a6df6bb604b37cda3723fe6b4b"
		priv = "0x" + rBytes.toString('hex')

		//Encrypt
		var ciphertext = cryptojs.AES.encrypt(priv,data.secret_key);
		console.log(priv);
		console.log(ciphertext.toString())

		User.findOne({sno : req.session.user.sno},function(err, user) {
			user.truffle_addr 	=	addr;
			user.secret_key 	= data.secret_key;
			user.save();

		})

		User.findOne({sno : req.session.user.sno},function(err, user) {

			// setup email data with unicode symbols
			let mailOptions = {
				from			:	'"DLT Labs" <hello@dltlabs.io>',
				to				:	 user.email,
				subject		:	'DLT ICO | Authorization Key',
				html			:	'Dear '+user.user_name+', <br/><br/>'+
										'Here is your authorization key:<br/><br/>'+
										ciphertext.toString()+'<br/><br/>'+
										'Please keep this handy while doing any transaction on DLT ICO.<br/><br/>'+
										'Regards <br/>Your Friends <br/>DLT Labs <br/><br/>'
			};

			// // send mail with defined transport object
			// mailer.transporter.sendMail(mailOptions, (error, info) => {
			// 	if (error) {
			// 		return console.log(error);
			// 	}
			// 	console.log('Message %s sent: %s', info.messageId, info.response);
			// });
		})

		req.session.user.truffle_addr = addr;
		return res.json({
			status : "success",
			message : "Your encrypted private key has been emailed to your account"
		})

	},
};
