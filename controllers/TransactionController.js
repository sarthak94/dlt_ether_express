const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const User = require('../models/user');
const users = require('./UserController');
const Transaction = require('../models/transaction');
const moment = require('moment');
const crypto = require('crypto');
const cryptojs = require("crypto-js");
const rand = require("random-seed").create()
const ethutil = require("ethereumjs-util")
const rpc = 'http://localhost:8545';
const BN = require("bn.js");
const TX = require("ethereumjs-tx");
const Web3 = require('web3');
var web3 = new Web3();

var provider = new Web3.providers.HttpProvider(rpc);
web3 = new Web3(provider);


String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g,  function(txt){return txt.substr(0).toLowerCase();} );
}

padLeft = function(n, width, z) {
	z = z || '0';
	n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

var self = module.exports = {

  //Preparing,signing and sending the transaction (transfer or purchase)
  send_transaction : function(sender, receiver, amount, gaslimit, ether_data, secret_key, encrypted_key, callback){

    /* Start preparing the transaction */
    var senderAccount = sender;
    var receiverAccount = receiver;
    var amount = amount;
    var gaslimit = gaslimit;
    var data = ether_data;

    //Lets get this party started..
    var tx = new TX();

    //set nonce
    var sender = senderAccount.toProperCase();
    var nonce = web3.eth.getTransactionCount(sender);
    tx.nonce = nonce;

    //set the gasprice
    var gasprice = web3.eth.gasPrice;
    tx.gasPrice = new BN(web3.eth.gasPrice.toString(10));

    //set the receiver address
    var receiver = receiverAccount.toProperCase();
    tx.to = receiver;

    //set transaction data
    if (data===""||data===undefined){
        tx.data = "0x";
    } else {
        tx.data = data;
    }

    //set the gas limit
    if (gaslimit===""||gaslimit===undefined){
        tx.gasLimit = tx.getBaseFee();
    } else {
        tx.gasLimit = new BN(gaslimit);
    }

    //prepping for the "all" command
    var txfee = tx.getUpfrontCost();
    balance = web3.eth.getBalance(sender);
    balance = new BN((balance.toString(10)));

    //Set the value (in wei) of the cryptocurrency payload of the transaction
    var val = new BN();

    if (amount == "all"){
        val = balance.sub(txfee);
    } else {

        val = new BN(amount.toString());

    }
    tx.value = val;

    //the gas cost of the tx, so we can check if we've allocated enough gas to execute the transaction
    var balRequired = new BN(0);
    balRequired = val.add(txfee);

    /* End preparing the transaction */

    if(balRequired.cmp(balance) > 0) {

        callback("insufficient");
    }
    else{

       /* Start signing the transaction */

       var rawtx = tx.serialize().toString("hex");
       var bytes  = cryptojs.AES.decrypt(encrypted_key.toString(),secret_key);
    	 var privateKey = bytes.toString(cryptojs.enc.Utf8);
       console.log(privateKey);

       var priv = new Buffer(ethutil.stripHexPrefix(privateKey), 'hex');
       var signedtx = new TX(rawtx);
       signedtx.sign(priv);
       var signedTransaction="0x" + signedtx.serialize().toString("hex");

       /* End signing the transaction */

       /* Start sending the signed transaction */

       var result = web3.eth.sendRawTransaction(signedTransaction);
       callback(result);

       /* End sending the signed transaction */
    }


 },

 // Fetching the wallet tokens, wallet amount (ether), truffle address, and all transactions of the user
 get_wallet : function(req, res){

   var functionSignature = "balanceOf(address)";
   var parameteres = [req.session.user.truffle_addr];
   var funcode = "0x" + ethutil.sha3(functionSignature).toString('hex').substr(0,8);
   var params = funcode;

   if(parameteres!=undefined){
        for(var i=0; i<parameteres.length; i++) {
            if(parameteres[i].toString().indexOf("0x")===-1){
                var p = padLeft(new BN(parameteres[i]).toString(16), 64);
                params += p;
            }else{
                var p1 = padLeft(ethutil.stripHexPrefix(parameteres[i]),64);
                params += p1;
            }

        }
    }
   balance = web3.eth.getBalance(req.session.user.truffle_addr);
   balance = new BN((balance.toString(10)));

   // var json = {
   //             "to":"0x0fb6451916771f4e8a4c108402d884a3bb4c5072",
   //             "data":params
   //           };
   // var tokens = web3.eth.call(json);

   // Transaction log entry for user
   users.transaction_history(req,function(transactions){
		 return res.render('mywallet',{wallet_amount:balance,address:req.session.user.truffle_addr,transactions:transactions})
	 });
 },


// Transfer of ethers to another user
 transfer_ether : function(req, res){

   var sender = req.session.user.truffle_addr;
   var contract = '0x0fb6451916771f4e8a4c108402d884a3bb4c5072';
   var encrypted_key = req.body.encrypted_key;
   var secret_key = req.body.secret_key;
   var to = req.body.address;
   var ethers = req.body.ethers;
   var gaslimit = 450000;

   User.findOne({truffle_addr:to},function(err,receiver){

     if(!receiver)
        return res.json({status:2});
     else
     {
       var functionSignature = "transfer(address,uint256)";
       var parameteres = [to,ethers];
       var funcode = "0x" + ethutil.sha3(functionSignature).toString('hex').substr(0,8);
       var params = funcode;

       if(parameteres!=undefined){
            for(var i=0; i<parameteres.length; i++) {
                if(parameteres[i].toString().indexOf("0x")===-1){
                    var p = padLeft(new BN(parameteres[i]).toString(16), 64);
                    params += p;
                }else{
                    var p1 = padLeft(ethutil.stripHexPrefix(parameteres[i]),64);
                    params += p1;
                }

            }
        }

        var ether_data = params;
        console.log("ok")
        // web3.personal.unlockAccount(sender);
        // console.log("okk")
        web3.eth.sendTransaction({from:sender,to:to,value:web3.toWei(ethers,"ether"),gas:'1000000'});
        var transaction = new Transaction({
              sender_sno : req.session.user.sno,
              receiver_sno : receiver.sno,
              sender_name : req.session.user.user_name,
              receiver_name : receiver.user_name,
              ethers    : ethers,
              type : 1,
              remarks : "Transfer",
              datetime : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
            });

            transaction.save(function(err) {
              if (err) throw err;
            });

            var transaction = new Transaction({
               sender_sno     : receiver.sno,
               receiver_sno   : req.session.user.sno,
               sender_name    : receiver.user_name,
               receiver_name  : req.session.user.user_name,
               ethers         : ethers,
               type           : 2,
               remarks        : "Transfer",
               datetime       : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
             });

             transaction.save(function(err) {
               if (err) throw err;
             });

          return res.json({status:1});

        // self.send_transaction(sender,contract,"0",gaslimit,ether_data,secret_key,encrypted_key,function(result){

        //   if(result == 'insufficient')
        //     return res.json({status:2});
        //   else
        //   {
        //      var transaction = new Transaction({
        //         sender_sno : req.session.user.sno,
        //         receiver_sno : receiver.sno,
        //         sender_name : req.session.user.user_name,
        //         receiver_name : receiver.user_name,
        //         ethers		:	ethers,
        //         type : 1,
        //         remarks : "Transfer",
        //         datetime : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
        //       });

        //       transaction.save(function(err) {
        //         if (err) throw err;
        //       });

        //       var transaction = new Transaction({
        //          sender_sno     : receiver.sno,
        //          receiver_sno   : req.session.user.sno,
        //          sender_name    : receiver.user_name,
        //          receiver_name  : req.session.user.user_name,
        //          ethers		      :	ethers,
        //          type           : 2,
        //          remarks        : "Transfer",
        //          datetime       : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
        //        });

        //        transaction.save(function(err) {
        //          if (err) throw err;
        //        });

        //     return res.json({status:1});
        //   }

        // });
      }

   });

 },

 // Purchase of ethers by the user
 buy_ether : function(req, res){

  // var sender = req.session.user.truffle_addr;
  var sender = "0xcd6618746525e602d2495a1ea6f2b3f9b2bb200b"
  var receiver = req.session.user.truffle_addr;
 	var contract = '0x0fb6451916771f4e8a4c108402d884a3bb4c5072';
 	var ethers = req.body.ethers;
 	var gaslimit = 450000;
 	var encrypted_key = req.body.encrypted_key;
 	var secret_key = req.body.secret_key;


  web3.personal.unlockAccount(sender, "123");
      web3.eth.sendTransaction({from:sender,to:receiver,value:web3.toWei(ethers,"ether"),gas:'1000000'});

      User.findOne({truffle_addr:req.session.user.truffle_addr},function(err, receiver){
        if (receiver)
        {
           receiver.transfers.push({ethers:ethers,datetime:moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')});
           receiver.save();

           var transaction = new Transaction({
              receiver_sno  : receiver.sno,
              receiver_name : receiver.user_name,
              ethers        : ethers,
              type          : 2,
              remarks       : "Purchase",
              datetime      : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
            });

            transaction.save(function(err) {
              if (err) throw err;
            });
        }
      });

      return res.json({status:1});
	// self.send_transaction(sender,contract,amount,gaslimit,"",secret_key,encrypted_key,function(result){

 //    if(result == 'insufficient')
 //      return res.json({status:2});
 //    else
 //    {
 //      web3.personal.unlockAccount(sender, "123");
 //      web3.eth.sendTransaction({from:sender,to:receiver,value:web3.toWei(ethers,"ether"),gas:'1000000'});

 //      User.findOne({truffle_addr:req.session.user.truffle_addr},function(err, receiver){
 //        if (receiver)
 //        {
 //           receiver.transfers.push({ethers:ethers,datetime:moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')});
 //           receiver.save();

 //           var transaction = new Transaction({
 //              receiver_sno  : receiver.sno,
 //              receiver_name : receiver.user_name,
 //              ethers		    :	ethers,
 //              type          : 2,
 //              remarks       : "Purchase",
 //              datetime      : moment(Math.floor(new Date().getTime())).format('Do MMM, YYYY h:mm a')
 //            });

 //            transaction.save(function(err) {
 //              if (err) throw err;
 //            });
 //        }
 //      });

 //      return res.json({status:1});
 //     }

	// });
 }

};
