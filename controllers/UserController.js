const mongoose = require('mongoose');
const User = require('../models/user');
const Transaction = require('../models/transaction');
const crypto = require('crypto');
const cryptojs = require("crypto-js");
const rand = require("random-seed").create()
const ethutil = require("ethereumjs-util")
const moment = require('moment');
// const mailer = require('../../mailer');


module.exports = {

	//Registration of user along with validations, sending registration mail to user
	create: function(req, res) {
		var data = req.body;
		User.findOne({email : data.email},function(err, users) {
			if (users){
				return res.json({
					status:"unsuccess",
					message:"OOps! email already exist, Please try again with another email",
					// data: users
				});
			} else {
				var current_date = (new Date()).valueOf().toString();
				var random = Math.random().toString();
				var unique_code = crypto.createHash('sha1').update(current_date + random).digest('hex');

				var user = new User({
					user_name	:	data.user_name,
					email		:	data.email,
					password	:	data.password,
					verified	:	0,
					token		:	unique_code,
					// role		:	data.role,
				});

				user.save(function(err) {
					if (err) throw err;
				});

				// // setup email data with unicode symbols
				// let mailOptions = {
				// 	from			:	 '"DLT Labs" <hello@dltlabs.io>',
				// 	to				:	 user.email,
				// 	subject		:	'Welcome to DLT ICO | Activate your account.',
				// 	html			:	'Dear '+user.user_name+', <br/><br/>'+
				// 							'Welcome aboard! Confirm your email id to activate your account.<br/><br/>'+
				// 							'Verification Link: <a href="http://ico.dltlabs.io/email-verification?token='+user.token+'">http://ico.dltlabs.io/email-verification?token='+user.token+'</a><br/><br/>'+
				// 							'If you didn\'t create a DLT ICO account, just delete this email and everything will go back to the way it was.<br/><br/>'+
				// 							'Regards <br/>Your Friends <br/>DLT Labs <br/><br/>'+
				// 							'------------------------------------------------------------------------------'
				// };

				// // send mail with defined transport object
				// mailer.transporter.sendMail(mailOptions, (error, info) => {
				// 	if (error) {
				// 		return console.log(error);
				// 	}
				// 	console.log('Message %s sent: %s', info.messageId, info.response);
				// });

				res.redirect('/login');
				// return res.json({
				// 	status:"success",
				// 	message:"user add successfully.",
				// 	data: user
				// });
			}
		});
	},

	//Authentication of user when the user logs in
	auth: function(req, res) {
		var data = req.body;
		User.findOne({email : data.email, password : data.password},function(err, user) {
			var response = {
				status:"unsuccess",
				message:"Credentials not correct.",
			}
			if (user) {
				if (user.verified == 1) {
						req.session.user = user;

						response = {
							status:"success",
							message:"Logged In successfully.",
							data: user
						}
						if (user.truffle_addr) {
							return res.redirect('/mywallet')
						} else {
							return res.redirect('/createwallet')
						}
					} else {
						response.message = "user not verified.";
					}
			}
			return res.json(response);
		});
	},

	//Verification of email of the user on the basis of token sent via email
	verifyEmail: function(req, res) {
		var token = req.param('token');
		User.findOne({token : token},function(err, user) {
			var response = {
				status:"404",
				message:"token not found.",
			}
			if (user) {
				var current_date = (new Date()).valueOf().toString();
				var random = Math.random().toString();
				var unique_code = crypto.createHash('sha1').update(current_date + random).digest('hex');
				user.token = unique_code;
				user.verified = 1;
				user.save();

				return res.redirect('/login?status='+"success")
			}
			return res.status(404).json(response);
		});
	},

	//Creation of truffle account of the user,sending encrypted authorization key via email
	truffleAcc: function(req, res) {
		// req.session.user
		var data = req.body;
		rand.addEntropy(data.sentence);
		var rBytes = new Buffer(32)
		for(var i = 0; i < 32; i++){
			rBytes[i] = rand(256)
		}

		addr = "0x" + ethutil.privateToAddress(rBytes).toString('hex')
		priv = "0x" + rBytes.toString('hex')

		//Encrypt
		var ciphertext = cryptojs.AES.encrypt(priv,data.secret_key);
		console.log(priv);

		//Decrypt
		// var bytes  = cryptojs.AES.decrypt(ciphertext.toString(),priv);
		// var plaintext = bytes.toString(cryptojs.enc.Utf8);

		User.findOne({sno : req.session.user.sno},function(err, user) {
			user.truffle_addr 	=	addr;
			user.secret_key 	= data.secret_key;
			user.save();
		})

		User.findOne({sno : req.session.user.sno},function(err, user) {

			// setup email data with unicode symbols
			let mailOptions = {
				from			:	 '"DLT Labs" <hello@dltlabs.io>',
				to				:	 user.email,
				subject		:	'DLT ICO | Authorization Key',
				html			:	'Dear '+user.user_name+', <br/><br/>'+
										'Here is your authorization key:<br/><br/>'+
										ciphertext.toString()+'<br/><br/>'+
										'Please keep this handy while doing any transaction on DLT ICO.<br/><br/>'+
										'Regards <br/>Your Friends <br/>DLT Labs <br/><br/>'
			};

			// // send mail with defined transport object
			// mailer.transporter.sendMail(mailOptions, (error, info) => {
			// 	if (error) {
			// 		return console.log(error);
			// 	}
			// 	console.log('Message %s sent: %s', info.messageId, info.response);
			// });
		})
		req.session.user.truffle_addr = addr;
		return res.redirect('/mywallet')

	},

	//Fetching details of the purchased ether history of the user
	ether_history: function(req, res) {

		User.findOne({sno:req.session.user.sno},function(err, ethers) {
			if (err)
				console.log(err);
			else
				return res.render('buy_ethers',{ethers:ethers});
		});

	},

	//Fetching details of the all the transaction history of the user (purchase and transfers)
	transaction_history: function(req, callback) {

	  Transaction.find({sender_sno : req.session.user.sno}).sort({datetime:-1}).exec(function(err, transaction) {
			if (err)
				console.log(err);
			else{
				console.log("transfer history");
				return callback(transaction);
				
			}
		});

	},

	//Fetching details of the ether transfer history of the user
	transfer_history : function(req, res){

    Transaction.find({sender_sno:req.session.user.sno,type:1}).sort({datetime:-1}).exec(function(err, transfer) {
      if (err)
        console.log(err);
      else
        return res.render('transfer_ether',{transfer:transfer});
    });

  }

};
