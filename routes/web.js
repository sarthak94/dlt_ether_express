var router = require('express').Router();
var bodyParser = require('body-parser');

const session = require('express-session')
const cookieParser = require('cookie-parser');
router.use(cookieParser());
var MemoryStore = session.MemoryStore;
router.use(session({
	secret: "1234567890QWERTY",
}))

router.use(function(req, res, next) {
	res.locals.user = req.session.user
	// res.session = req.session
	next()
});

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));


var users = require('../controllers/UserController');
var Transaction = require('../controllers/TransactionController');


router.get('/',function(req, res) {
	res.redirect('/login')
});

router.get('/register',function(req, res) {
	res.render('register');
});

router.post('/register', users.create);

router.get('/email-verification', users.verifyEmail);

router.get('/login',function(req, res) {
	res.render('index');
});

router.post('/login', users.auth);

router.get('/profile', function(req, res) {
	res.render('profile');
});

router.get('/editProfile', function(req, res) {
	res.render('edit-profile');
});

router.get('/createwallet', function(req, res) {
	res.render('create_truffle');
});
router.post('/createwallet', users.truffleAcc);

router.get('/myhome',function(req, res) {
	res.render('dashboard');
});

router.get('/mywallet',Transaction.get_wallet);

router.get('/buyEther',users.ether_history);

router.post('/buyEther',Transaction.buy_ether);

router.get('/transferEther',users.transfer_history);

router.post('/transferEther',Transaction.transfer_ether);

router.get('/authorizedPerson',function(req, res) {
	res.render('authorize');
});

router.get('/logout',function(req, res) {
	req.session.destroy();
	res.redirect('/login')
});


module.exports = router;
