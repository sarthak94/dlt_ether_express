var router = require('express').Router();
var bodyParser = require('body-parser');

var fileUpload = require('express-fileupload');
router.use(fileUpload());

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));


var users = require('../controllers/Api/UserController');

router.post('/register', users.create);

// router.get('/email-verification', users.verifyEmail);

router.post('/login', users.auth);
router.post('/editProfile', users.profile);
router.post('/editPic', users.ProfileImage);
router.post('/create-truffle-acc', users.truffleAcc);

module.exports = router;
