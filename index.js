var PORT = 80;
var express = require('express');
var app = express();
var Entities = require('html-entities').XmlEntities;
var rand = require("random-seed").create();
var ethutil = require("ethereumjs-util");
var BN = require("bn.js");
var TX = require("ethereumjs-tx");
var rpc = 'http://localhost:8545';
var Web3 = require('web3');
var web3 = new Web3();

var provider = new Web3.providers.HttpProvider(rpc);
web3 = new Web3(provider);



entities = new Entities();


String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g,  function(txt){return txt.substr(0).toLowerCase();} );
}

app.post('/test', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var tdata = body.testdata;
            console.log('TestData: ' + tdata);

            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                TestData: tdata
            }));
        });
});


app.post('/generateAccount', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var entropy = body.randomEntropy;
            console.log('randomEntropy: ' + entropy);

            rand.addEntropy("This is random string to generate address");
            var rBytes = new Buffer(32)
            for(var i = 0; i < 32; i++){
            rBytes[i] = rand(256)
            }
            addr = "0x" + ethutil.privateToAddress(rBytes).toString('hex')
            priv = "0x" + rBytes.toString('hex')
            console.log("\n\naddress:\n" + addr)
            console.log("\n\nprivate key:\n" + priv + "\n\n")

            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                Address: addr,
                PrivateKey: priv
            }));
        });
});

app.post('/generateData', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var functionSignature = body.functionSignature;
            var parameteres=body.parameters;
            console.log('functionSignature: ' + functionSignature);
            console.log('parameteres: ' + parameteres);
            var funcode = "0x" + ethutil.sha3(functionSignature).toString('hex').substr(0,8);
            var params = funcode;
            if(parameteres!=undefined){
                for(var i=0; i<parameteres.length; i++) {
                    var p = padLeft(new BN(parameteres[i]).toString(16), 64);
                    params += p;
                    console.log("Adding encoded parameter ---->  " + p);
                }
            }
            
           
            console.log("\n\nfunction code:\n" + funcode + "\n\n");
            console.log("\nThe txdata required to make this call is:\n");
            console.log(params);
            console.log("\n");
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                functionCode: funcode,
                transactionData: params
            }));
        });
});


padLeft = function(n, width, z) {
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}


app.post('/prapareTransaction', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var senderAccount = body.sender;
            var receiverAccount = body.receiver;
            var amount = body.amount;
            var gaslimit = body.gaslimit;
            var data = body.data;
            console.log('sender: ' + senderAccount);
            console.log('receiver: ' + receiverAccount);
            console.log('amount: ' + amount);
            console.log('gaslimit: ' + gaslimit);
            console.log('data: ' + data);

            //Lets get this party started..
            var tx = new TX();

            //set nonce
            var sender = senderAccount.toProperCase();
            var nonce = web3.eth.getTransactionCount(sender);
            tx.nonce = nonce;

            //set the gasprice
            var gasprice = web3.eth.gasPrice;
            tx.gasPrice = new BN(web3.eth.gasPrice.toString(10));

            //set the receiver address
            var receiver = receiverAccount.toProperCase();
            tx.to = receiver;

            //set transaction data
            if (data===""||data===undefined){
                tx.data = "0x";
            } else {
                tx.data = data;
            }

            //set the gas limit
            if (gaslimit===""||gaslimit===undefined){
                tx.gasLimit = tx.getBaseFee();
            } else {
                tx.gasLimit = new BN(gaslimit);
            }

            //prepping for the "all" command
            var txfee = tx.getUpfrontCost();
            balance = web3.eth.getBalance(sender);
            balance = new BN((balance.toString(10)));

            //Set the value (in wei) of the cryptocurrency payload of the transaction
            var val = new BN();
            if (amount == "all"){
                val = balance.sub(txfee);
            } else {
                val = new BN(amount);
            }
            tx.value = val;

            console.log("\n\nJSON of the unsigned transaction:\n");
            console.log( tx.toJSON(true) );

            console.log("\n\nraw unsigned transaction:\n");
            console.log("0x" + tx.serialize().toString("hex") + "\n\n");

            //the gas cost of the tx, so we can check if we've allocated enough gas to execute the transaction
            var balRequired = new BN(0);
            balRequired = val.add(txfee);

            if(balRequired.cmp(balance) > 0) {
                console.log("\n\nYour account has insufficient balance to send the balance.\n");
                console.log("\nBalance:");
                console.log(balance.toString(10));
                console.log("\nRequired balance:");
                console.log(balRequired.toString(10));
                console.log("\nDifference balance in Wei:");
                console.log(balRequired.sub(balance).toString());
                console.log("\nDifference balance in gas:");
                console.log((balRequired.sub(balance)).div(new BN(tx.gasPrice)).toString());

                res.writeHead(200, {
                'Content-Type': 'application/json'
                });
                res.end(JSON.stringify({
                    status: 'insufficient balance',
                    Balance: balance.toString(10),
                    RequiredBalance: balRequired.toString(10)
                }));

            }
            else{
                res.writeHead(200, {
                'Content-Type': 'application/json'
                });
                res.end(JSON.stringify({
                    status: 'success',
                    JSONUnsignedTransaction: tx.toJSON(true),
                    RawUnsignedTransaction: tx.serialize().toString("hex")
                }));
            }

            
        });
});

app.post('/signTransaction', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var rawtx  = body.rawTransaction;
            var privateKey  = body.privateKey;
            console.log('rawtx: ' + rawtx);
            console.log('privateKey: ' + privateKey);
            var priv = new Buffer(ethutil.stripHexPrefix(privateKey), 'hex');
            var signedtx = new TX(rawtx);
            signedtx.sign(priv);
            var signedTransaction="0x" + signedtx.serialize().toString("hex");
            console.log("signedTransaction :"+ signedTransaction);

            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                signedTransaction: signedTransaction
            }));
        });
});

app.post('/sendTransaction', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            var rawSignedTransaction  = body.rawSignedTransaction;
            console.log('rawSignedTransaction: ' + rawSignedTransaction);
            var result=web3.eth.sendRawTransaction(rawSignedTransaction);

            console.log("Result :"+ result);
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                result: result
            }));
        });
});

app.post('/getData', function (req, res) {

  
        var jsonString = '';

        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {

            console.log('Received Payload Body : ' + jsonString);
            var body = JSON.parse(jsonString);
            
            var result=web3.eth.call(body);
            console.log("Result :"+ result);
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                result: result
            }));
        });
});
       
        app.get('/test/:testdata', function (req, res) {

            var tdata = req.params.testdata;
            console.log('received lat: ' + tdata);

            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify({
                status: 'success',
                TestData: tdata

            }));
        });
		
		app.get('/testarray/:testdata', function (req, res) {

            var tdata = req.params.testdata;

            console.log('received lat: ' + tdata);
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify(
			[
			{
                status: 'success',
                TestData: '1 '+tdata

            },
			{
                status: 'success',
                TestData: '2 '+tdata

            },
			{
                status: 'success',
                TestData: '3 '+tdata

            }
			]));
        });
		
		
		app.get('/testspread/:testdata', function (req, res) {

            var tdata = req.params.testdata;

            console.log('received lat: ' + tdata);
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.end(JSON.stringify(
			{
			title: 'TestingSpread',
			description: 'this is to test spread',
			comment : {
				data: [
					{
                status: 'success',
                TestData: '1 '+tdata,
				value: '1umesh'

            },
			{
                status: 'success',
                TestData: '2 '+tdata,
				value: '2umesh'

            },
			{
                status: 'success',
                TestData: '3 '+tdata,
				value: '3umesh'

            }
				]
			}
			}
			));
        });


        app.listen(PORT, function () {
            console.log('app listening on port ', PORT);
        });
