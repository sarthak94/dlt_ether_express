const express 	= require('express');
const fileUpload = require('express-fileupload');
const mongoose 	= require('mongoose');

mongoose.Promise 	= global.Promise;
mongoose.connect('mongodb://localhost:27017/dlt_ether');

// Init app
var app = express();
global.price_per_token = 0.0005;

// view engine setup
app.set('views', 'views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// set static Folder
app.use(express.static('public'));

var routes  = require('./routes/web');
var api 	= require('./routes/api');


app.use('/', routes);
app.use('/api', api);

// default options 
app.use(fileUpload());

// Handle 404
app.use(function(req, res) {
 res.status(404);
res.render('404', {title: '404: File Not Found'});
});

// Handle 500
app.use(function(error, req, res, next) {
 res.status(505);
 // res.send({error:error});
// res.render('505', {title:'505: Internal Server Error', error: error});
});

var port = 3000;

app.listen(port, function() {
	console.log("App Started on PORT 3000");
});
