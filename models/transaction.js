var mongoose = require('mongoose');
// var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var transactionSchema = new Schema({
	sender_sno: {type : Number},
  receiver_sno: {type : Number},
  sender_name: {type : String},
  receiver_name: {type : String},
	ethers: { type: Number },
  type : { type: Number },
  remarks: { type: String },
	datetime : {type : String}
});

var Transaction = mongoose.model('Transaction', transactionSchema);
module.exports = Transaction;
