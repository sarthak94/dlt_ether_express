var mongoose = require('mongoose');
// var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

// User Schema
var userSchema = new Schema({
	sno: { type: Number },
	user_name: { type: String, required: true },
	email: { type: String, required: true },
	password : { type: String },
	profile	 : { type: String},
	// role:{ type:String },
	amount: { type: Number },
	transfers: { type: Array },
	token: 			{ type: String },
	truffle_addr: 	{ type: String },
	secret_key: 	{ type: String },
	verified: 		{ type: Number },
	created_at: Date,
	updated_at: Date
});



// on every save, add the date
userSchema.pre('save', function(next) {
	// get the current date
	var currentDate = new Date();
	// change the updated_at field to current date
	this.updated_at = currentDate;
	// if created_at doesn't exist, add to that field
	if (!this.created_at) {
		this.created_at = currentDate;
		var sno = 1;
		var user = this;
		User.find({}, function(err, users) {
		if (err) throw err;
			sno = users.length + 1;
			user.sno = sno;
			next();
		});
	}
	else{
		next();
	}

});


// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;
